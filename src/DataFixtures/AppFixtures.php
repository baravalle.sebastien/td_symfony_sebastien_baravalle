<?php

namespace App\DataFixtures;

use App\Entity\Constant;
use App\Entity\Bien;
use App\Entity\Famille;
use App\Entity\Reservation;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Role;
use App\Entity\Contact;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {
        
        /***
         *  ETAPE DE CREATION D'ENTITE:
         * 
         *      1. USER---> CONTACT                USER (admin/ proprietaire)
         *          |  |--> ROLE (client)           |
         *          |                               |
         *          |--->RESERVATION--->FAMILLE     |
         *                          |-->BIEN <------|
         *                          
         */         



        $faker = Factory::create();
        //Constante
        $constante = new Constant();
            $constante->setKeyConst("type:M-H 3")->setValueConst("20.00");
        $manager->persist($constante);
        $constante2 = new Constant();
            $constante2->setKeyConst("type:M-H 4")->setValueConst("24.00");
        $manager->persist($constante2);
        $constante3 = new Constant();
            $constante3->setKeyConst("type:M-H 5")->setValueConst("27.00");
        $manager->persist($constante3);
        $constante4 = new Constant();
            $constante4->setKeyConst("type:M-H 6-8")->setValueConst("34.00");
        $manager->persist($constante4);
        $constante5 = new Constant();
            $constante5->setKeyConst("type:Caravane 2 places ")->setValueConst("15.00");
        $manager->persist($constante5);
        $constante6 = new Constant();
            $constante6->setKeyConst("type:Caravane 4 places")->setValueConst("18.00");
        $manager->persist($constante6);
        $constante7 = new Constant();
            $constante7->setKeyConst("type:Carvane 6 places")->setValueConst("24.00");
        $manager->persist($constante7);
        $constante8 = new Constant();
            $constante8->setKeyConst("type:emplacement 8m²")->setValueConst("12.00");
        $manager->persist($constante8);
        $constante9 = new Constant();
            $constante9->setKeyConst("type:emplacement 12m²")->setValueConst("14.00");
        $manager->persist($constante9);
        $constante10 = new Constant();
            $constante10->setKeyConst("taxe:enfant")->setValueConst("0.35");
        $manager->persist($constante10);
        $constante11 = new Constant();
            $constante11->setKeyConst("taxe:adulte")->setValueConst("0.60");
        $manager->persist($constante11);
        $constante12 = new Constant();
            $constante12->setKeyConst("piscine:enfant")->setValueConst("1.00");
        $manager->persist($constante12);
        $constante13 = new Constant();
            $constante13->setKeyConst("piscine:adulte")->setValueConst("1.50");
        $manager->persist($constante13);
        $constante14 = new Constant();
            $constante14->setKeyConst("date:debut-saison")->setValueConst("05-05-YYYY");
        $manager->persist($constante14);
        $constante15 = new Constant();
            $constante15->setKeyConst("date:debut-haute")->setValueConst("21-06-YYYY");
        $manager->persist($constante15);
        $constante16 = new Constant();
            $constante16->setKeyConst("date:fin-haute")->setValueConst("31-08-YYYY");
        $manager->persist($constante16);
        $constante17 = new Constant();
            $constante17->setKeyConst("date:fin-saison")->setValueConst("10-10-YYYY");
        $manager->persist($constante17);



        //ROLES:
        $role_administrateur= new Role();
        $role_administrateur->setTitle('ROLE_ADMIN');
        $manager->persist($role_administrateur);
        $role_client= new Role();
        $role_client->setTitle('ROLE_CLIENT');
        $manager->persist($role_client);
        $role_proprietaire= new Role();
        $role_proprietaire->setTitle('ROLE_PROPRIETAIRE');
        $manager->persist($role_proprietaire);



        // USERS :
        /**
         * POUR CHACUN
         *  1- creer User,
         *  2- Le persister -> id
         *  3- Le lier a Son role
         *  4- persister le role
         *  5- creer son contact
         *  6- le lier a contact
         *  7- persister
         */
        //  -admin:
        $admin = new User();
        $hash = $this->encoder->encodePassword($admin, 'admin');
        $admin->setLogin('admin')
            ->setHash($hash);
        $manager->persist($admin);
        $role_administrateur->addUser($admin);
        $manager->persist($role_administrateur);
        $admin_contact= new Contact();
        $admin_contact->setNom('DuCamping')
            ->setPrenom('Jean')
            ->setEmail('lespadrillevollante@camping.com')
            ->setTelephone('0674213212')
            ->setUser($admin);
        $manager->persist($admin_contact);

        //  -proprietaire:
        $proprietaire = new User();
        $hash = $this->encoder->encodePassword($proprietaire, 'proprietaire');
        $proprietaire->setLogin('proprietaire')
            ->setHash($hash);
        $manager->persist($proprietaire);
        $role_proprietaire->addUser($proprietaire);
        $manager->persist($role_proprietaire);
        $proprietaire_contact= new Contact();
        $proprietaire_contact->setNom($faker->lastName())
            ->setPrenom($faker->firstName())
            ->setEmail($faker->email())
            ->setTelephone($faker->phoneNumber())
            ->setUser($proprietaire);
        $manager->persist($proprietaire_contact);




        //  -proprietaire:
        $proprietaire_2 = new User();
        $hash = $this->encoder->encodePassword($proprietaire_2, 'proprietaire_2');
        $proprietaire_2->setLogin('proprietaire_2')
            ->setHash($hash);
        $manager->persist($proprietaire_2);
        $role_proprietaire->addUser($proprietaire_2);
        $manager->persist($role_proprietaire);
        $proprietaire_2_contact= new Contact();
        $proprietaire_2_contact->setNom($faker->lastName())
            ->setPrenom($faker->firstName())
            ->setEmail($faker->email())
            ->setTelephone($faker->phoneNumber())
            ->setUser($proprietaire_2);
        $manager->persist($proprietaire_2_contact);



        //  -proprietaire:
        $proprietaire_3 = new User();
        $hash = $this->encoder->encodePassword($proprietaire_3, 'proprietaire_3');
        $proprietaire_3->setLogin('proprietaire_3')
            ->setHash($hash);
        $manager->persist($proprietaire_3);
        $role_proprietaire->addUser($proprietaire_3);
        $manager->persist($role_proprietaire);
        $proprietaire_3_contact= new Contact();
        $proprietaire_3_contact->setNom($faker->lastName())
            ->setPrenom($faker->firstName())
            ->setEmail($faker->email())
            ->setTelephone($faker->phoneNumber())
            ->setUser($proprietaire_3);
        $manager->persist($proprietaire_3_contact);


        $tab_proprietaire=[$admin, $admin, $admin, $proprietaire,$proprietaire_2, $proprietaire_3];
        $tab_type=[$constante2 ,$constante3 ,$constante4 ,$constante5 ,$constante6,$constante7,$constante8,$constante9,$constante ];
        for($i=0; $i<15; $i++){
            $prop= rand(0,5);
            $type=rand(0,8);
            // INTEGRER BIEN
            $bien = new Bien();
            $bien->setType($tab_type[$type])
                ->setProprietaire( $tab_proprietaire[$prop])
                ->setTitre(' l\'emplacement '.$i)
                ->setDescription($faker->paragraph());
            $manager->persist($bien);
            $this->addReference("bien-" . $i, $bien);

        }

        //  -client:
        for($i=0; $i < 10; $i++){

            $client = new User();
            $hash = $this->encoder->encodePassword($client, 'client_'.$i);
            $client->setLogin('client_'.$i)
                ->setHash($hash);
            $manager->persist($client);
            $role_client->addUser($client);
            $manager->persist($role_client);
            $client_contact= new Contact();
            $client_contact->setNom($faker->lastName())
                ->setPrenom($faker->firstName())
                ->setEmail($faker->email())
                ->setTelephone($faker->phoneNumber())
                ->setUser($client);
            $manager->persist($client_contact);


            // INTEGRER FAMILLE
            $famille= new Famille();
                $nbEnfant = rand(-2,4);
                $nbEnfant= $nbEnfant<0 ? 0 : $nbEnfant;
                $nbAdulte = rand(1,4);
                $randDuration = mt_rand(1,15);


                $famille->setJoursAdultes($nbAdulte * $randDuration)
                    ->setJoursEnfants($nbEnfant * $randDuration)
                    ->setJoursPiscineAdultes(rand(0, ($nbAdulte * $randDuration)))
                    ->setJoursPiscineEnfants(rand(0,($nbEnfant * $randDuration) ));
            $manager->persist($famille);

            // FINALISER: TOUT LIER DANS RESERVATION
            $reservation = new Reservation();
            $reservation->setFamille($famille)
                ->setBien($this->getReference('bien-'.rand(0,11)))
                ->setUser($client);
                $dateDebut = $faker->dateTimeBetween('+7 months', '+12 months');
                $reservation
                    ->setDateDebut($dateDebut)
                    ->setDateFin((clone $dateDebut)->modify("+$randDuration days"));
            $manager->persist($reservation);
        }    

        $manager->flush();


     
    }
}
