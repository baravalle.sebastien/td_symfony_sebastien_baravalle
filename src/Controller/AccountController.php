<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\FamilleType;
use App\Form\RegistrationType;
use App\Form\ReservationType;
use App\Repository\FamilleRepository;
use App\Repository\ReservationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Repository\RoleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
class AccountController extends AbstractController
{
    
    /**
     * @Route("/login", name="account_login", methods={"GET", "POST"})
     */
    public function login(AuthenticationUtils $utils)
    {
        $error = $utils->getLastAuthenticationError();
        $username = $utils->getLastUsername();
        return $this->render('account/login.html.twig',[
            'hasError'=> $error !== null,
            'username'=> $username
        ]
    
    );
    }
    /**
     * @Route("/logout", name="account_logout", methods={"GET", "POST"})
     */
    public function logout()
    {
    }

    /**
     * @Route("/register", name="account_register")
     * 
     */
    public function register(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder, RoleRepository $role_rep){
        $user= new User();

        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $hash= $encoder->encodePassword($user, $user->getHash());
            $roleClient = $role_rep->findOneByTitle('ROLE_CLIENT');
            $user->setUserRole($roleClient);
            $user->setHash($hash);
            $manager->persist($user);
            $roleClient->addUser($user);

            $manager->persist($roleClient);
            $manager->flush();
            dump($form);

            $this->addFlash(
                'success',
                ' Utilisateur <strong>'.$user->getLogin().'</strong> Enregistré ! '
            );

            return $this->redirectToRoute('account_login');
        }

        return $this->render('account/registration.html.twig',[
            'form'=> $form->createView()
        ]);
    }

    /**
     *  FormulaireDeModification Exemple undone
     * @Route("/modification", name="ex_modification")
     * 
     */
    public function Modification( ReservationRepository $repo_reservation){

        $user = $this->getUser();
        $reservation= $repo_reservation->findOneBy(["user" =>  $user] );

        $dump= $reservation;
        $dump=$reservation->getBien()->getType();
        // a partir de User Trouver Famille
        // $form = $this->createForm(FamilleType::class, $famille);
        return $this->render('dump.html.twig',[
            "dump"=>$dump
        ]);
    }
    /**
     *  CHANGES RESERVATION MODIFIE:
     *      -les info de famille
     *      -les info de reservation
     *  I. a condition que l'user ai une reservation
     *  
     * @Route("/changes_reservation", name="accompt_changes_reservation")
     * @IsGranted("ROLE_CLIENT")
     * 
     */
    public function changesReservation(  EntityManagerInterface $manager,ReservationRepository $repo_reservation, Request $request){

        $user = $this->getUser();
        $reservation= $repo_reservation->findOneBy(["user" =>  $user] );
        $bien = $reservation->getBien();
        $reservation->setPrice();
        $price = $reservation->getPrice();
        

        //  .I
        if( $reservation != null){

            $form = $this->createForm(ReservationType::class, $reservation);
    
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                
                $manager->persist($reservation);
                $manager->flush();
    
                $this->addFlash(
                    'success',
                    'Reservation mise a jour '
                );
                
                return $this->redirectToRoute('account_show_facture');
            }
            
            return $this->render('home/reservation.html.twig',[
                'oldReservation'=>true,
                'prix'=>  $price,
                'bien'=>$bien,
                'form'=>$form->createView()
            ]);

        }
       

    }
    /**
     * @Route("/show-facture", name="account_show_facture")
     * @IsGranted("ROLE_CLIENT")
     */
    public function showFacture(ReservationRepository $repo_reservation){
        $user = $this->getUser();
        $reservation= $repo_reservation->findOneBy(["user" =>  $user] );
        $reservation->setPrice();
        return $this->render('account/show-facture.html.twig',[
            'reservation' => $reservation,
        ]);

    }



}
