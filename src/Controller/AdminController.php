<?php

namespace App\Controller;

use App\Entity\Bien;
use App\Entity\User;
use App\Form\FamilleType;
use App\Form\RegistrationType;
use App\Form\ReservationType;
use App\Repository\FamilleRepository;
use App\Repository\ReservationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Repository\RoleRepository;
use App\Repository\ConstantRepository;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Form\BienType;
use App\Entity\Role;
use App\Repository\UserRepository;

class AdminController extends AbstractController
{

    
    // TODO: CREATION D'USER PROPRIETAIRE; MODIFICATION DE MOT DE PASSE CONTACT
     
    /**
     * CREATION D'ANNONCE PAR L'ADMIN
     * 2possibilitées:
     *      1. ADMIN EST PROPRIETAIRE:
     *      2. SINON HIDDEN INPUT RENVOIS ID d'aprés liste des propriétaire.
     *
     * @Route("/admin_add_bien", name="admin_add_bien")
     * @IsGranted("ROLE_ADMIN")
     */
    public function addBien(Request $request, EntityManagerInterface $manager, RoleRepository $role_repo, UserRepository $user_repo, ConstantRepository $const_repo)
    {
        //2. Preparer tableau de liste des propriétaire
        $proprietaires= $role_repo->findOneByTitle('ROLE_PROPRIETAIRE');
        $proprietaires= $proprietaires->getUsers();


        //LISTE DES TYPES
        $listeType = $const_repo->returnType();
        
        $bien = new Bien();
        $form= $this->createForm(BienType::class, $bien);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            //1. SI un PROPRIETAIRE ID A UNE VALUE 
            //  ON RECUPERE LE PROPRIETAIRE
            $all = $request->request->all();
            $proprietaire_id= $all['bien']['proprietaire_id'];
            $indexChoosenType= $all['bien']['type_index'];
            $indexChoosenType -=1;
            dump($proprietaire_id);
            if($proprietaire_id = null || $proprietaire_id ==""){
                $user= $this->getUser();
            }else{
                $user = $user_repo->findOneById($proprietaire_id);
                if(!$user){
                    $user= $this->getUser();
                }

            }
            $bien->setProprietaire($user);
            $bien->setType($listeType[$indexChoosenType]);
            $manager->persist($bien);
            $manager->flush();
            $this->addFlash(
                'success',
                'Reservation enregistrée '
            );

            return $this->redirectToRoute('home'); 

        }


        return $this->render('admin/add-bien.html.twig', [
            'form' => $form->createView(),
            'listType'=> $listeType,
            'proprietaires'=>$proprietaires
        ]);
    }

    /**
     * @Route("/all-reservations", name="admin_all_reservations")
     * @IsGranted("ROLE_ADMIN")
     * 
     */
    public function allReservation(ReservationRepository $repo){
        $reservations = $repo->findAll();
        foreach($reservations as $reservation){
            $reservation->setPrice();
            
        }
        return $this->render('admin/all-reservation.html.twig', [
            'reservations'=> $reservations
        ]);

    }

}
