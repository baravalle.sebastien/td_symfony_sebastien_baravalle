<?php

namespace App\Controller;

use App\Repository\BienRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Reservation;
use App\Form\ReservationType;
use App\Entity\Bien;
use App\Entity\Famille;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ReservationRepository;
class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home(BienRepository $repo_bien)
    {
        $biens = $repo_bien->findAll();
        return $this->render('home/home.html.twig', [
            'biens' => $biens,
        ]);
    }
    /**
     * Booking est lancé quand on clique sur le "RESERVER"
     * d'une annonce 
     *  @Route("/booking/{bien_id}",  name="booking")
     *  @IsGranted("ROLE_CLIENT")
     */
    public function booking($bien_id, EntityManagerInterface $manager, Request $request, BienRepository $bienRepository, ReservationRepository $repo_reservation){
        $user = $this->getUser();

        $reservation= $repo_reservation->findOneBy(["user" =>  $user] );
        if($reservation == null) $reservation = new Reservation();
        $bien = $bienRepository->findOneById($bien_id);
        $reservation->setBien($bien);

        $form = $this->createForm(ReservationType::class, $reservation);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            

            if(!$reservation->isBookableDates()){
                dump(1);
                $this->addFlash(
                    'warning',
                    ' les dates choisies ne sont pas disponibles'
                );
            }else{
                dump(2);
                $famille = $reservation->getFamille();

                $manager->persist($famille);
                $reservation->setBien($bien)
                    ->setUser($user);
                $manager->persist($reservation);
                $manager->flush();
                $this->addFlash(
                    'success',
                    'Reservation enregistrée '
                );
    
                // return $this->redirectToRoute('account_show_facture');

            }


        }


        return $this->render('home/reservation.html.twig', [
            'prix'=> 0,
            'oldReservation'=>0,
            'bien'=>$bien,
            'form'=>$form->createView(),
            
        ]); 


    }
}
