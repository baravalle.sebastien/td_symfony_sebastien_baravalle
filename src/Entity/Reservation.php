<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ReservationRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Reservation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * 
     */
    private $date_debut;

    /**
     * @ORM\Column(type="datetime")
     * 

     */
    private $date_fin;

    /**
     * @ORM\ManyToOne(targetEntity=Famille::class, inversedBy="reservations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $famille;

    /**
     * @ORM\ManyToOne(targetEntity=Bien::class, inversedBy="reservations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bien;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reservations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function isBookableDates(){
        //1. impossible pour annonce
        $notAvailableDays = $this->bien->getNotAvailableDays();

        // comparaison: 
        $bookingDays = $this->getReservationDays();
        $days = array_map(function($day){
            return $day->format('Y-m-d');
        }, $bookingDays);

        $notAvailableDays = array_map(function($day){
            return $day->format('Y-m-d');
        }, $notAvailableDays);

        dump(["jours indisponibles"=> $notAvailableDays]);
        dump(["jours de la reservation" => $days]);
        foreach($days as$day){
            if(array_search($day, $notAvailableDays) !== false){
                dump("FAUX");
                return false;
            }else{
                
                dump('iteration');
            }
        }
        return true;

    }
    public function getReservationDays(){

        $debutTime = $this->date_debut->getTimestamp();
        $finTime = $this->date_fin->getTimestamp();
        $dayTime = 1*60*60*24;
        $result = range($debutTime, $finTime, $dayTime );
        $days = array_map(function( $dayTimestamp){
            return new \DateTime(date('Y-m-d',$dayTimestamp));
        }, $result);
        return $days;

    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->date_debut;
    }

    public function setDateDebut(\DateTimeInterface $date_debut): self
    {
        $this->date_debut = $date_debut;
        

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->date_fin;
    }

    public function setDateFin(\DateTimeInterface $date_fin): self
    {
        $this->date_fin = $date_fin;

        return $this;
    }

    public function getFamille(): ?Famille
    {
        return $this->famille;
    }

    public function setFamille(?Famille $famille): self
    {
        $this->famille = $famille;

        return $this;
    }

    public function getBien(): ?Bien
    {
        return $this->bien;
    }

    public function setBien(?Bien $bien): self
    {
        $this->bien = $bien;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }


    /*
        CALCUL DU PRIX 
    */
    private $price = [] ;
    /**
     * @ORM\PrePersist
     */
    public function setPrice(){
        $date_d = clone($this->date_debut);
        $date_f = clone($this->date_fin);
        $date_d = $date_d->getTimestamp();
        $date_f = $date_f->getTimestamp();

        $nbJours = abs( $date_f- $date_d );
        $nbJours= round( $nbJours/60/60/24);

        $JoursAdultes = $this->famille->getJoursAdultes();
        $JoursEnfants = $this->famille->getJoursEnfants();
        $JoursAdultesPiscine = $this->famille->getJoursPiscineAdultes();
        $JoursEnfantsPiscine = $this->famille->getJoursPiscineEnfants();
        $prixBien = $this->bien->getPrice();
        $nbAdulte = $JoursAdultes/ $nbJours;
        $prix = $this->getDaysPeerSaeson($date_d, $date_f, $prixBien); 
        $this->price['prixHT'] = $prix;
        $charges=[
            'taxeAdulte'=>( $JoursAdultes * 0.6),
            'taxeEnfant'=>($JoursEnfants * 0.35),
            'piscineAdulte'=>($JoursAdultesPiscine* 1.5),
            'piscineEnfant'=>($JoursEnfantsPiscine * 1.0),
            'totalCharges'=>(
                (( $JoursAdultes * 0.6)+($JoursEnfants * 0.35)+($JoursAdultesPiscine* 1.5)+($JoursEnfantsPiscine * 1.0))
            )
            
        ];
        $this->price['charges']=$charges;
        $prix += $JoursAdultes * 0.6;
        $prix += $JoursEnfants * 0.35;
        $prix += $JoursEnfantsPiscine * 1.0;
        $prix += $JoursAdultesPiscine * 1.5;
        $this->price['total']=$prix;


    }
    public function getDaysPeerSaeson($date_d, $date_f, $prixBien){
        $currentYear = $this->date_debut->format('Y');
        $haute_d = strtotime('06/20/'.$currentYear);
        $haute_f=strtotime('08/31/'.$currentYear);

        dump([
            "debut reservation" =>$date_d,
            "fin reservation"=> $date_f,
            "debut haute saison "=> $haute_d,
            "fin haute saison "=> $haute_f
        ]);
        
        if($date_f > $haute_d){

            $j_avant =( $haute_d- $date_d ) ;
            $j_avant = $j_avant >0 ? $j_avant : 0;
            $j_avant= round( $j_avant/60/60/24);

        }else{
            $j_avant=($date_f-$date_d);
            $j_avant= round( $j_avant/60/60/24);

            $j_pendant = 0;
            $j_apres = 0;
        }
        
        if($date_d > $haute_f){
            $j_pendant = 0;
            $j_apres=($date_f-$date_d);
            $j_apres = $j_apres >0 ? $j_apres : 0;
            $j_apres=round( $j_apres/60/60/24);


        }else{
            $j_apres=($date_f-$haute_f);
            $j_apres = $j_apres >0 ? $j_apres : 0;
            $j_apres=round( $j_apres/60/60/24);
            
            $j_pendant=($date_f-$date_d);
            $j_pendant= round( $j_pendant/60/60/24);
            $j_pendant= $j_pendant- ($j_apres + $j_avant);
            $j_pendant = $j_pendant >0 ? $j_pendant : 0;

        }    



        $detailSaison=[
            "jouravant"=> $j_avant,
            "jourpendant"=> $j_pendant,
            "jourapres"=> $j_apres

        ];
        $this->price["detailSaison"]= $detailSaison;
        return $this->doFacture($j_avant, $j_pendant, $j_apres, $prixBien);
    }

    public function doFacture($j_avant, $j_pendant, $j_apres, $prixloc){
        $tout_j = $j_avant+$j_pendant+ $j_apres;
        $prixT = 0;
        $taux = 0.05;
        $tauxAplique = 0;
        $rabais = 0;
        $jCount = 0;
        $prixNormal= $prixloc;
        $nbSemaine = floor($tout_j/7); 
        $semaineCount = 0;
        $dumpInfo = 0;
        for($i = 0 ; $i < $tout_j; $i++){
            $dumpInfo+=1;
            if($i< $j_avant){
                $prixloc = $prixNormal;
            }else if($i< ($j_avant+$j_pendant)){
                $prixloc = $prixNormal+($prixNormal * 0.15);

            }else if( $i< ($j_avant+$j_pendant+$j_apres)){
                $prixloc = $prixNormal;

            } 
             // CALCUL RABAIS:
            /*
                Chaque jour jCount + 1, au bout d'une semaine
                > le 5% de remise est appliqué une fois de plus
                sur le jour
            */
            for($j=0; $j<$tauxAplique; $j++){
                $rabais+= ($prixloc*$taux);
            }
            $remiseSemaine = $rabais *7;
            $prixT += $prixloc-$rabais;
            $rabais = 0;
            $jCount += 1;

            if($jCount == 7){
                $semaineCount += 1;
                $tauxAplique+=1;
                $jCount = 0;
            }
            
            
            
            
        
        }
        dump([
            'nb iteration '=>$dumpInfo
        ]);
        return $prixT;

    }
    public function getPrice()
    {
        return $this->price;
    }

}
