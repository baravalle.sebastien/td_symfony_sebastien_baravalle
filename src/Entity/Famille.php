<?php

namespace App\Entity;

use App\Repository\FamilleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FamilleRepository::class)
 */
class Famille
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $jours_enfants;

    /**
     * @ORM\Column(type="integer")
     */
    private $jours_adultes;

    

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $jours_piscine_enfants;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $jours_piscine_adultes;

    /**
     * @ORM\OneToMany(targetEntity=Reservation::class, mappedBy="famille")
     */
    private $reservations;

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJoursEnfants(): ?int
    {
        return $this->jours_enfants;
    }

    public function setJoursEnfants(?int $jours_enfants): self
    {
        $this->jours_enfants = $jours_enfants;

        return $this;
    }

    public function getJoursAdultes(): ?int
    {
        return $this->jours_adultes;
    }

    public function setJoursAdultes(int $jours_adultes): self
    {
        $this->jours_adultes = $jours_adultes;

        return $this;
    }

    public function getJoursPiscineEnfants(): ?int
    {
        return $this->jours_piscine_enfants;
    }

    public function setJoursPiscineEnfants(?int $jours_piscine_enfants): self
    {
        $this->jours_piscine_enfants = $jours_piscine_enfants;

        return $this;
    }

    public function getJoursPiscineAdultes(): ?int
    {
        return $this->jours_piscine_adultes;
    }

    public function setJoursPiscineAdultes(?int $jours_piscine_adultes): self
    {
        $this->jours_piscine_adultes = $jours_piscine_adultes;

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setFamille($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->contains($reservation)) {
            $this->reservations->removeElement($reservation);
            // set the owning side to null (unless already changed)
            if ($reservation->getFamille() === $this) {
                $reservation->setFamille(null);
            }
        }

        return $this;
    }
}
