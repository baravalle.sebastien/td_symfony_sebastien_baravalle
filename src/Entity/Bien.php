<?php

namespace App\Entity;

use App\Repository\BienRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass=BienRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Bien
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    private $price;

    public function getPrice(){
        $price= $this->type->getValueConst();
        return $price;

    }

   

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="biens")
     */
    private $proprietaire;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Reservation::class, mappedBy="bien")
     */
    private $reservations;

    /**
     * @ORM\ManyToOne(targetEntity=Constant::class, inversedBy="use_by")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    public function __construct()
    {
        
        $this->reservations = new ArrayCollection();
    }

    /**
     * @return array 
     */
    public function getNotAvailableDays(){
        $notAvailablesDays = [];
        foreach($this->reservations as $reservation ){
            $debutTime = $reservation->getDateDebut()->getTimestamp();
            $finTime = $reservation->getDateFin()->getTimestamp();
            $dayTime = 1*60*60*24;
            $result = range($debutTime, $finTime, $dayTime );
            $days = array_map(function( $dayTimestamp){
                return new \DateTime(date('Y-m-d',$dayTimestamp));
            }, $result);
            $notAvailablesDays = array_merge($notAvailablesDays, $days);
        }
        return $notAvailablesDays;
    }
    public function getId(): ?int
    {
    
        return $this->id;

    }

    public function getType(): ?Constant{
        return $this->type;

    }
    private $categorie;
    public function getCategorie(): ?string
    {
        $type= $this->type-> getKeyConst();
        $type= str_replace ( 'type:' , '' , $type );
        return $type;
    }

    public function setType(Constant $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getProprietaire(): ?User
    {
        return $this->proprietaire;
    }

    public function setProprietaire(?User $proprietaire): self
    {
        $this->proprietaire = $proprietaire;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setBien($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->contains($reservation)) {
            $this->reservations->removeElement($reservation);
            // set the owning side to null (unless already changed)
            if ($reservation->getBien() === $this) {
                $reservation->setBien(null);
            }
        }

        return $this;
    }
}
