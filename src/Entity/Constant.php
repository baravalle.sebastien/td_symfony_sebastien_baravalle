<?php

namespace App\Entity;

use App\Repository\ConstantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConstantRepository::class)
 */
class Constant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $key_const;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $value_const;

    /**
     * @ORM\OneToMany(targetEntity=Bien::class, mappedBy="type")
     */
    private $use_by;

    public function __construct()
    {
        $this->use_by = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKeyConst(): ?string
    {
        return $this->key_const;
    }

    public function setKeyConst(string $key_const): self
    {
        $this->key_const = $key_const;

        return $this;
    }

    public function getValueConst(): ?string
    {
        return $this->value_const;
    }

    public function setValueConst(string $value_const): self
    {
        $this->value_const = $value_const;

        return $this;
    }

    /**
     * @return Collection|Bien[]
     */
    public function getUseBy(): Collection
    {
        return $this->use_by;
    }

    public function addUseBy(Bien $useBy): self
    {
        if (!$this->use_by->contains($useBy)) {
            $this->use_by[] = $useBy;
            $useBy->setType($this);
        }

        return $this;
    }

    public function removeUseBy(Bien $useBy): self
    {
        if ($this->use_by->contains($useBy)) {
            $this->use_by->removeElement($useBy);
            // set the owning side to null (unless already changed)
            if ($useBy->getType() === $this) {
                $useBy->setType(null);
            }
        }

        return $this;
    }
}
