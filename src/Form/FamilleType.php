<?php

namespace App\Form;

use App\Entity\Famille;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FamilleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('jours_enfants', IntegerType::class,['attr'=>
            ['class'=>'nocalcul']
        ])
            ->add('jours_adultes',IntegerType::class,['attr'=>
            ['class'=>'nocalcul']
        ])
            ->add('jours_piscine_enfants',IntegerType::class,['attr'=>
            ['class'=>'nocalcul']
        ])
            ->add('jours_piscine_adultes',IntegerType::class,['attr'=>
            ['class'=>'nocalcul']
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Famille::class,
        ]);
    }
}
