<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use App\Entity\Bien;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BienType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('proprietaire_id', HiddenType::class ,[
                'mapped'=>false
            ])
            ->add('type_index', HiddenType::class,[
                'mapped'=>false
            ])
            ->add('titre')
            ->add('description')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Bien::class,
        ]);
    }
}
