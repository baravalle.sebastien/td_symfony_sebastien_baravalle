<?php

namespace App\Form;
use App\Form\ApplicationType;

use App\Entity\Reservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
class ReservationType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date_debut',DateTimeType::class,
            $this->getConfiguration('date d\'arrivée','Bonjour', [
                'attr'=>[
                    'class'=>'calcul'
                ]
            ])
            )
            ->add('date_fin',DateTimeType::class,$this->getConfiguration('date de départ','', [
                'attr'=>[
                    'class'=>'calcul'
                ]
            ]))
            ->add('famille', FamilleType::class, ['attr'=>
                ['class'=>'no-calcul']
            ])
            ->add('bien_id', HiddenType::class ,[
                'mapped'=>false

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
