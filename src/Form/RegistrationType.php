<?php

namespace App\Form;
use App\Form\ApplicationType;

use App\Entity\User;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use App\Form\ContactType;

use Symfony\Component\OptionsResolver\OptionsResolver;
class RegistrationType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('login', TextType::class, 
            $this->getConfiguration('Login', 'Votre nom d\'utilisateur' )
            )
            ->add('hash', PasswordType::class,
            $this->getConfiguration('mot de passe', 'Votre mot de passe')
            )
            ->add('passwordConfirm',PasswordType::class, $this->getConfiguration('Confirmer mot de passe', 'Veuillez confirmer'))
            ->add('contact',ContactType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
