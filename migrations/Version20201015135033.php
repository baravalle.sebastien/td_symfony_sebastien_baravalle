<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201015135033 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bien (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, proprietaire_id INTEGER DEFAULT NULL, type_id INTEGER NOT NULL, titre VARCHAR(60) NOT NULL, description VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE INDEX IDX_45EDC38676C50E4A ON bien (proprietaire_id)');
        $this->addSql('CREATE INDEX IDX_45EDC386C54C8C93 ON bien (type_id)');
        $this->addSql('CREATE TABLE constant (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, key_const VARCHAR(60) NOT NULL, value_const VARCHAR(60) NOT NULL)');
        $this->addSql('CREATE TABLE contact (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER NOT NULL, nom VARCHAR(60) NOT NULL, prenom VARCHAR(60) NOT NULL, email VARCHAR(255) NOT NULL, telephone VARCHAR(10) NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4C62E638A76ED395 ON contact (user_id)');
        $this->addSql('CREATE TABLE famille (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, jours_enfants INTEGER DEFAULT NULL, jours_adultes INTEGER NOT NULL, jours_piscine_enfants INTEGER DEFAULT NULL, jours_piscine_adultes INTEGER DEFAULT NULL)');
        $this->addSql('CREATE TABLE reservation (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, famille_id INTEGER NOT NULL, bien_id INTEGER NOT NULL, user_id INTEGER NOT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL)');
        $this->addSql('CREATE INDEX IDX_42C8495597A77B84 ON reservation (famille_id)');
        $this->addSql('CREATE INDEX IDX_42C84955BD95B80F ON reservation (bien_id)');
        $this->addSql('CREATE INDEX IDX_42C84955A76ED395 ON reservation (user_id)');
        $this->addSql('CREATE TABLE role (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_role_id INTEGER NOT NULL, login VARCHAR(30) NOT NULL, hash VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_8D93D6498E0E3CA6 ON user (user_role_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE bien');
        $this->addSql('DROP TABLE constant');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE famille');
        $this->addSql('DROP TABLE reservation');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE user');
    }
}
